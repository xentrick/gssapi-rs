extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
  let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
  println!("cargo:rustc-link-lib=gssapi_krb5");

  let gssapi = bindgen::Builder::default()
    .header("includes/gssapi.h")
    .hide_type("max_align_t")
    .generate()
    .expect("Unable to generate <gssapi.h> bindings");
  gssapi.write_to_file(out_path.join("gssapi.rs")).expect("Couldn't write <gssapi.h> bindings!");
  
  /*
  let gssapi_generic = bindgen::Builder::default()
    .header("includes/gssapi_generic.h")
    .generate()
    .expect("Unable to generate <gssapi_generic.h> bindings");
  gssapi_generic.write_to_file(out_path.join("gssapi_generic.rs")).expect("Couldn't write <gssapi_generic.h> bindings!");
  
  let gssapi_ext = bindgen::Builder::default()
    .header("includes/gssapi_ext.h")
    .generate()
    .expect("Unable to generate <gssapi_ext.h> bindings");
  gssapi_ext.write_to_file(out_path.join("gssapi_ext.rs")).expect("Couldn't write <gssapi_ext.h> bindings!");
  
  let gssapi_krb5 = bindgen::Builder::default()
    .header("includes/gssapi_krb5.h")
    .generate()
    .expect("Unable to generate <gssapi_krb5.h> bindings");
  gssapi_krb5.write_to_file(out_path.join("gssapi_krb5.rs")).expect("Couldn't write <gssapi_krb5.h> bindings!");
  
  let gssapi_mech = bindgen::Builder::default()
    .header("includes/gssapi_mech.h")
    .generate()
    .expect("Unable to generate <mechglue.h> bindings");
  gssapi_mech.write_to_file(out_path.join("gssapi_mech.rs")).expect("Couldn't write <mechglue.h> bindings!");
  */
}
