### gssapi-sys
#### Rust FFI bindings to gssapi via bindgen
#### Spenser Reinhardt <commiebstrd@protonmail.com>

This is separate from the crates.io version of gssapi-sys. This is intended to work with the gssapi safe rust code included in this repository.
