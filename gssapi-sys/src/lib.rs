#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/gssapi.rs"));

/*
// ignoring for now, gssapi seems to include everything we need and cross compiles better

pub mod gssapi;
pub mod generic;
pub mod ext;
pub mod krb5;
pub mod mechglue;
*/

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
