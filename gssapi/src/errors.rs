#[allow(unused_imports)] use num::{FromPrimitive,ToPrimitive};
use ::types::OM_uint32;

pub const GSS_C_SUPPLEMENTARY_OFFSET: u32 = 0;
pub const GSS_C_ROUTINE_ERROR_OFFSET: u32 = 16;
pub const GSS_C_CALLING_ERROR_OFFSET: u32 = 24;
pub const GSS_C_SUPPLEMENTARY_MASK: u32 = 0177777;
pub const GSS_C_ROUTINE_ERROR_MASK: u32 = 0377;
pub const GSS_C_CALLING_ERROR_MASK: u32 = 0377;

enum_from_primitive! {
  pub enum GSS_Status_Code {
    Complete            = 0,
    // Suplementary
    ContinueNeeded      = 1 << 0,
    DuplicateToken      = 1 << 1,
    OldToken            = 1 << 2,
    UnseqToken          = 1 << 3,
    GapToken            = 1 << 4,
    // Routine Errors
    BadMech             = 1 << GSS_C_ROUTINE_ERROR_OFFSET,
    BadName             = 2 << GSS_C_ROUTINE_ERROR_OFFSET,
    NameType            = 3 << GSS_C_ROUTINE_ERROR_OFFSET,
    BadBindings         = 4 << GSS_C_ROUTINE_ERROR_OFFSET,
    BadStatus           = 5 << GSS_C_ROUTINE_ERROR_OFFSET,
    BadSig              = 6 << GSS_C_ROUTINE_ERROR_OFFSET, // BadMic
    NoCred              = 7 << GSS_C_ROUTINE_ERROR_OFFSET,
    NoContext           = 8 << GSS_C_ROUTINE_ERROR_OFFSET,
    DefectiveToken      = 9 << GSS_C_ROUTINE_ERROR_OFFSET,
    DefectiveCredential = 10 << GSS_C_ROUTINE_ERROR_OFFSET,
    CredentialsExpired  = 11 << GSS_C_ROUTINE_ERROR_OFFSET,
    ContextExpired      = 12 << GSS_C_ROUTINE_ERROR_OFFSET,
    Failure             = 13 << GSS_C_ROUTINE_ERROR_OFFSET,
    BadQOP              = 14 << GSS_C_ROUTINE_ERROR_OFFSET,
    Unauthorized        = 15 << GSS_C_ROUTINE_ERROR_OFFSET,
    Unavailable         = 16 << GSS_C_ROUTINE_ERROR_OFFSET,
    DuplicateElement    = 17 << GSS_C_ROUTINE_ERROR_OFFSET,
    NameNotMN           = 18 << GSS_C_ROUTINE_ERROR_OFFSET,
    // Calling Errors
    InaccessibleRead    = 1 << GSS_C_CALLING_ERROR_OFFSET,
    InaccessibleWrite   = 2 << GSS_C_CALLING_ERROR_OFFSET,
    BadStructure        = 3 << GSS_C_CALLING_ERROR_OFFSET,
    // not gssapi compliant, for From conversions
    UnknownError        = 0xffffffff
  }
}
impl From<OM_uint32> for GSS_Status_Code {
  fn from(val: OM_uint32) -> GSS_Status_Code {
    let mut res = GSS_Status_Code::UnknownError;
    if let Some(v) = GSS_Status_Code::get_supplementary_info(val) {
      res = v;
    }
    if let Some(v) = GSS_Status_Code::get_routine_error(val) {
      res = v;
    }
    if let Some(v) = GSS_Status_Code::get_call_error(val) {
      res = v;
    }
    res
  }
}
impl GSS_Status_Code {
  pub fn get_supplementary_info(status: OM_uint32) -> Option<GSS_Status_Code> {
    let mask = GSS_C_SUPPLEMENTARY_MASK << GSS_C_SUPPLEMENTARY_OFFSET;
    GSS_Status_Code::from_u32(status & mask)
  }
  pub fn get_routine_error(status: OM_uint32) -> Option<GSS_Status_Code> {
    let mask = GSS_C_ROUTINE_ERROR_MASK << GSS_C_ROUTINE_ERROR_OFFSET;
    GSS_Status_Code::from_u32(status & mask)
  }
  pub fn get_call_error(status: OM_uint32) -> Option<GSS_Status_Code> {
    let mask = GSS_C_CALLING_ERROR_MASK << GSS_C_CALLING_ERROR_OFFSET;
    GSS_Status_Code::from_u32(status & mask)
  }
  pub fn get_gss_error(status: OM_uint32) -> Option<GSS_Status_Code> {
    let mask = (GSS_C_CALLING_ERROR_MASK << GSS_C_CALLING_ERROR_OFFSET) | (GSS_C_ROUTINE_ERROR_MASK << GSS_C_ROUTINE_ERROR_OFFSET);
    GSS_Status_Code::from_u32(status & mask)
  }
}
