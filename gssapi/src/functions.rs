use ::types::*;
use ::constants::*;
use ::errors::*;

use gssapi_sys;

pub fn gss_import_name(
  minor_status: &mut OM_uint32,
  input_name_buffer: &mut gss_buffer_desc_struct,
  input_name_type: &mut gss_oid_desc_struct,
  output_name: &mut gss_name_struct,
) -> OM_uint32 {

  let major_status = unsafe{
    gssapi_sys::gss_import_name(
      minor_status,
      &mut input_name_buffer.to_sys(),
      &mut input_name_type.to_sys(),
      &mut output_name.to_sys_ptr(),
    )
  };

  major_status
}

pub fn gss_acquire_cred(
  minor_status: &mut OM_uint32,                // status on return, probably doesnt need to be arg
  desired_name: &mut gss_name_struct,          // gss_import_name() || GSS_C_NO_NAME
  time_req: OM_uint32,                         // time in seconds || GSS_C_INDEFINITE (max allowed)
  desired_mechs: &mut gss_oid_set_desc_struct, // desired authentication || GSS_C_NO_OID_SET (auto)
  cred_usage: gss_cred_usage_t,                // GSS_C_INITIATE || GSS_C_ACCEPT || GSS_C_BOTH
  output_cred_handle: &mut gss_cred_id_struct, // TODO: returned creds, probably doesnt need to be arg
  actual_mechs: &mut gss_oid_set_desc_struct,  // mechs that can be used with cred || NULL
  time_rec: &mut OM_uint32,                    // time cred is valid for || NULL
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_acquire_cred(
      minor_status,
      desired_name.to_sys_ptr(),
      time_req,
      desired_mechs.to_sys_ptr(),
      cred_usage as i32,
      &mut output_cred_handle.to_sys_ptr(),
      &mut actual_mechs.to_sys_ptr(),
      time_rec
    )
  };

  major_status
}

pub fn gss_release_cred(minor_status: OM_uint32, cred_handle: gss_cred_id_t)
  -> OM_uint32
{
  unimplemented!{};
}

// lib/context.c:28-430
pub fn gss_init_sec_context(
  minor_status: &mut OM_uint32,
  initiator_cred_handle: &mut gss_cred_id_struct,
  context_handle: &mut gss_ctx_id_struct,
  target_name: &mut gss_name_struct,
  mech_type: &mut gss_oid_desc_struct,
  req_flags: OM_uint32,
  time_req: OM_uint32,
  input_chan_bindings: &mut gss_channel_bindings_struct,
  input_token: &mut gss_buffer_desc_struct,
  actual_mech_type: &mut gss_oid_desc_struct,
  output_token: &mut gss_buffer_desc_struct,
  ret_flags: &mut OM_uint32,
  time_rec: &mut OM_uint32
) -> OM_uint32 {

  let major_status = unsafe{
    gssapi_sys::gss_init_sec_context(
      minor_status,
      initiator_cred_handle.to_sys_ptr(),
      &mut context_handle.to_sys_ptr(),
      target_name.to_sys_ptr(),
      mech_type.to_sys_ptr(),
      req_flags,
      time_req,
      input_chan_bindings.to_sys_ptr(),
      input_token.to_sys_ptr(),
      &mut actual_mech_type.to_sys_ptr(),
      output_token.to_sys_ptr(),
      ret_flags,
      time_rec
    )
  };

  major_status
}

pub fn gss_accept_sec_context(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  acceptor_cred_handle: &mut gss_cred_id_struct,
  input_token_buffer: &mut gss_buffer_desc_struct,
  input_chan_bindings: &mut gss_channel_bindings_struct,
  src_name: &mut gss_name_struct,
  mech_type: &mut gss_oid_desc_struct,
  output_token: &mut gss_buffer_desc_struct,
  ret_flags: &mut OM_uint32,
  time_rec: &mut OM_uint32,
  delegated_cred_handle: &mut gss_cred_id_struct
) -> OM_uint32 {

  let major_status = unsafe{
    gssapi_sys::gss_accept_sec_context(
      minor_status,
      &mut context_handle.to_sys_ptr(),
      acceptor_cred_handle.to_sys_ptr(),
      input_token_buffer.to_sys_ptr(),
      input_chan_bindings.to_sys_ptr(),
      &mut src_name.to_sys_ptr(),
      &mut mech_type.to_sys_ptr(),
      output_token.to_sys_ptr(),
      ret_flags,
      time_rec,
      &mut delegated_cred_handle.to_sys_ptr(),
    )
  };

  major_status
}

pub fn gss_process_context_token(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  token_buffer: &mut gss_buffer_desc_struct
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_process_context_token(
      minor_status,
      context_handle.to_sys_ptr(),
      token_buffer.to_sys_ptr(),
    )
  };

  major_status
}

pub fn gss_delete_sec_context(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  output_token: &mut gss_buffer_desc_struct
) -> OM_uint32 {
  unimplemented!{};
}

pub fn gss_context_time(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  time_rec: &mut OM_uint32
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_context_time(
      minor_status,
      context_handle.to_sys_ptr(),
      time_rec
    )
  };

  major_status
}

pub fn gss_get_mic(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_desc,
  qop_req: gss_qop_t,
  message_buffer: &mut gss_buffer_desc_struct,
  message_token: &mut gss_buffer_desc_struct
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_get_mic(
      minor_status,
      context_handle.to_sys_ptr(),
      qop_req,
      message_buffer.to_sys_ptr(),
      message_token.to_sys_ptr()
    )
  };

  major_status
}

pub fn gss_verify_mic(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  message_buffer: &mut gss_buffer_desc_struct,
  token_buffer: &mut gss_buffer_desc_struct,
  qop_state: &mut gss_qop_t
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_verify_mic(
      minor_status,
      context_handle.to_sys_ptr(),
      message_buffer.to_sys_ptr(),
      token_buffer.to_sys_ptr(),
      qop_state
    )
  };

  major_status
}

pub fn gss_wrap(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  conf_req_flag: i32,
  qop_req: gss_qop_t,
  input_message_buffer: &mut gss_buffer_desc_struct,
  conf_state: &mut i32,
  output_message_buffer: &mut gss_buffer_desc_struct
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_wrap(
      minor_status,
      context_handle.to_sys_ptr(),
      conf_req_flag,
      qop_req,
      input_message_buffer.to_sys_ptr(),
      conf_state,
      output_message_buffer.to_sys_ptr()
    )
  };

  major_status
}

pub fn gss_unwrap(
  minor_status: &mut OM_uint32,
  context_handle: &mut gss_ctx_id_struct,
  output_message_buffer: &mut gss_buffer_desc_struct,
  input_message_buffer: &mut gss_buffer_desc_struct,
  conf_state: &mut i32,
  qop_state: &mut gss_qop_t
) -> OM_uint32 {
  let major_status = unsafe{
    gssapi_sys::gss_unwrap(
      minor_status,
      context_handle.to_sys_ptr(),
      output_message_buffer.to_sys_ptr(),
      input_message_buffer.to_sys_ptr(),
      conf_state,
      qop_state
    )
  };

  major_status
}
