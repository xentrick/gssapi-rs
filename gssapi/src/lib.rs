#[macro_use] extern crate enum_primitive;
#[macro_use] extern crate lazy_static;
extern crate num;
extern crate gssapi_sys;

mod errors;
mod constants;
mod types;
mod functions;

pub mod prelude {
  pub use ::errors::*;
  pub use ::constants::*;
  pub use ::types::*;
  pub use ::functions::*;
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
