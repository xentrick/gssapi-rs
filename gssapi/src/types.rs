use std::ffi::CStr;
use std::os::raw::c_char;
use std::os::raw::c_void;

use ::constants::*;
use gssapi_sys;

pub type OM_uint32 = u32;
pub type gss_uint32 = OM_uint32;
pub type gss_qop_t = OM_uint32;
pub type gss_cred_usage_t = isize;

pub struct gss_oid_desc_struct {
  length: OM_uint32,
  elements: Vec<u8>,
}
pub type gss_oid_desc = gss_oid_desc_struct;
pub type gss_oid = *mut gss_oid_desc_struct;
impl gss_oid_desc_struct {
  pub fn new<S>(elems: S) -> gss_oid_desc_struct
    where S: Into<Vec<u8>>
  {
    let elems: Vec<u8> = elems.into();
    gss_oid_desc_struct {
      length: elems.len() as u32,
      elements: elems,
    }
  }
  pub fn to_sys(&mut self) -> gssapi_sys::gss_OID_desc_struct {
    gssapi_sys::gss_OID_desc_struct {
      length: self.length,
      elements: self.elements.as_mut_ptr() as *mut c_void,
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_OID {
    &mut self.to_sys()
  }
  pub fn from_sys(oid: gssapi_sys::gss_OID_desc_struct) -> gss_oid_desc_struct {
    if oid.length == 0 {
      gss_oid_desc_struct {
        length: 0,
        elements: Vec::with_capacity(0),
      }
    } else {
      let len: usize = oid.length as usize;
      gss_oid_desc_struct {
        length: oid.length,
        elements: unsafe{ Vec::from_raw_parts(oid.elements as *mut u8, len, len) },
      }
    }
  }
  pub fn from_sys_ptr(oid: gssapi_sys::gss_OID) -> gss_oid_desc_struct {
    let oid: gssapi_sys::gss_OID_desc_struct = unsafe{ *oid };
    gss_oid_desc_struct::from_sys(oid)
  }
}

pub struct gss_oid_set_desc_struct {
  count: OM_uint32,
  elements: Vec<gss_oid_desc>,
}
pub type gss_oid_set_desc = gss_oid_set_desc_struct;
pub type gss_oid_set = *mut gss_oid_set_desc_struct;
impl gss_oid_set_desc_struct {
  /* pub fn new() */
  pub fn to_sys(&mut self) -> gssapi_sys::gss_OID_set_desc_struct {
    let mut elements: Vec<gssapi_sys::gss_OID> = Vec::with_capacity(self.count as usize);
    for e in &mut self.elements {
      elements.push(e.to_sys_ptr());
    }
    gssapi_sys::gss_OID_set_desc_struct {
      count: self.count as usize,
      elements: elements.as_mut_ptr() as *mut gssapi_sys::gss_OID,
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_OID_set {
    &mut self.to_sys()
  }
  pub fn from_sys(set: gssapi_sys::gss_OID_set_desc_struct) -> gss_oid_set_desc_struct {
    if set.count == 0 {
      gss_oid_set_desc_struct {
        count: 0,
        elements: Vec::with_capacity(0),
      }
    } else {
      let length = set.count;
      let elements: Vec<gssapi_sys::gss_OID> = unsafe{ Vec::from_raw_parts(set.elements, length, length) };
      let elements: Vec<gss_oid_desc_struct> = elements.iter()
        .map(|e| gss_oid_desc_struct::from_sys_ptr(*e) )
        .collect();
      gss_oid_set_desc_struct {
        count: elements.len() as u32,
        elements: elements,
      }
    }
  }
  pub fn from_sys_ptr(set: gssapi_sys::gss_OID_set) -> gss_oid_set_desc_struct {
    let set: gssapi_sys::gss_OID_set_desc_struct = unsafe{ *set };
    gss_oid_set_desc_struct::from_sys(set)
  }
}

pub struct gss_buffer_desc_struct {
  length: usize,
  value: Vec<u8>
}
pub type gss_buffer_desc = gss_buffer_desc_struct;
pub type gss_buffer_t = *mut gss_buffer_desc_struct;
impl gss_buffer_desc_struct {
  pub fn new<V>(value: V) -> gss_buffer_desc_struct
    where V: Into<Vec<u8>>
  {
    let value: Vec<u8> = value.into();
    gss_buffer_desc_struct {
      length: value.len(),
      value: value,
    }
  }
  pub fn to_sys(&mut self) -> gssapi_sys::gss_buffer_desc_struct {
    gssapi_sys::gss_buffer_desc_struct {
      length: self.length,
      value: self.value.as_mut_ptr() as *mut c_void,
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_buffer_t {
  &mut self.to_sys()
  }
  pub fn from_sys(buf: gssapi_sys::gss_buffer_desc_struct) -> gss_buffer_desc_struct {
    if buf.length == 0 {
      gss_buffer_desc_struct {
        length: 0,
        value: Vec::with_capacity(0),
      }
    } else {
      let len: usize = buf.length as usize;
      gss_buffer_desc_struct {
        length: len,
        value: unsafe{ Vec::from_raw_parts(buf.value as *mut u8, len, len) },
      }
    }
  }
  pub fn from_sys_ptr(buf: gssapi_sys::gss_buffer_t) -> gss_buffer_desc_struct {
    let buf: gssapi_sys::gss_buffer_desc_struct = unsafe{ *buf };
    gss_buffer_desc_struct::from_sys(buf)
  }
}

// lib/internal.h
pub struct gss_name_struct {
  length: usize,
  value: Vec<u8>,
  oid_type: gss_oid_desc_struct, // to/from sys should be gss_oid (want real types w/ storage)
}
pub type gss_name_desc = gss_name_struct;
pub type gss_name_t = *mut gss_name_struct;
impl gss_name_struct {
  /* pub fn new() */
  pub fn to_sys(&mut self) -> gssapi_sys::gss_name_struct {
    gssapi_sys::gss_name_struct {
      length: self.length,
      value: self.value.as_mut_ptr() as *mut c_char,
      type_: self.oid_type.to_sys_ptr(),
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_name_t {
    &mut self.to_sys()
  }
  pub fn from_sys(name: gssapi_sys::gss_name_struct) -> gss_name_struct {
    if name.length == 0 {
      gss_name_struct {
        length: 0,
        value: Vec::with_capacity(0),
        oid_type: gss_oid_desc_struct::new(Vec::with_capacity(0)),
      }
    } else {
      gss_name_struct {
        length: name.length,
        value: unsafe{ Vec::from_raw_parts(name.value as *mut u8, name.length, name.length) },
        oid_type: gss_oid_desc_struct::from_sys_ptr(name.type_),
      }
    }
  }
  pub fn from_sys_ptr(name: gssapi_sys::gss_name_t) -> gss_name_struct {
    let name = unsafe{ *name };
    gss_name_struct::from_sys(name)
  }
}

pub struct gss_cred_id_struct {
  mech: gss_oid_desc_struct
  // krb5: gss_krb5_cred_struct // if use kerberos
}
pub type gss_cred_id_desc = gss_cred_id_struct;
pub type gss_cred_id_t = *mut gss_cred_id_struct;
impl gss_cred_id_struct {
  /* pub fn new() */
  pub fn to_sys(&mut self) -> gssapi_sys::gss_cred_id_struct {
    gssapi_sys::gss_cred_id_struct {
      mech: self.mech.to_sys_ptr(),
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_cred_id_t {
    &mut self.to_sys()
  }
  pub fn from_sys(cred: gssapi_sys::gss_cred_id_struct) -> gss_cred_id_struct {
    gss_cred_id_struct {
      mech: gss_oid_desc_struct::from_sys_ptr(cred.mech),
    }
  }
  pub fn from_sys_ptr(cred: gssapi_sys::gss_cred_id_t) -> gss_cred_id_struct {
    let cred: gssapi_sys::gss_cred_id_struct = unsafe{ *cred };
    gss_cred_id_struct::from_sys(cred)
  }
}

pub struct gss_ctx_id_struct {
  mech: gss_oid_desc_struct
  // krb5: gss_krb5_ctx_struct // if use kerberos
}
pub type gss_ctx_id_desc = gss_ctx_id_struct;
pub type gss_ctx_id_t = *mut gss_ctx_id_struct;
impl gss_ctx_id_struct {
  /* pub fn new() */
  pub fn to_sys(&mut self) -> gssapi_sys::gss_ctx_id_struct {
    gssapi_sys::gss_ctx_id_struct {
      mech: self.mech.to_sys_ptr(),
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_ctx_id_t {
    &mut self.to_sys()
  }
  pub fn from_sys(ctx: gssapi_sys::gss_ctx_id_struct) -> gss_ctx_id_struct {
    gss_ctx_id_struct {
      mech: gss_oid_desc_struct::from_sys_ptr(ctx.mech),
    }
  }
  pub fn from_sys_ptr(ctx: gssapi_sys::gss_ctx_id_t) -> gss_ctx_id_struct {
    let ctx: gssapi_sys::gss_ctx_id_struct = unsafe{ *ctx };
    gss_ctx_id_struct::from_sys(ctx)
  }
}

pub struct gss_channel_bindings_struct {
  initiator_addrtype: OM_uint32,
  initiator_address: gss_buffer_desc,
  acceptor_addrtype: OM_uint32,
  acceptor_address: gss_buffer_desc,
  application_data: gss_buffer_desc,
}
pub type gss_channel_bindings_desc = gss_channel_bindings_struct; // possible unnecessary
pub type gss_channel_bindings_t = *mut gss_channel_bindings_struct;
impl gss_channel_bindings_struct {
  /* pub fn new() */
  pub fn to_sys(&mut self) -> gssapi_sys::gss_channel_bindings_struct {
    gssapi_sys::gss_channel_bindings_struct {
      initiator_addrtype: self.initiator_addrtype,
      initiator_address: self.initiator_address.to_sys(),
      acceptor_addrtype: self.acceptor_addrtype,
      acceptor_address: self.acceptor_address.to_sys(),
      application_data: self.application_data.to_sys(),
    }
  }
  pub fn to_sys_ptr(&mut self) -> gssapi_sys::gss_channel_bindings_t {
    &mut self.to_sys()
  }
  pub fn from_sys(chan: gssapi_sys::gss_channel_bindings_struct) -> gss_channel_bindings_struct {
    gss_channel_bindings_struct {
      initiator_addrtype: chan.initiator_addrtype,
      initiator_address: gss_buffer_desc_struct::from_sys(chan.initiator_address),
      acceptor_addrtype: chan.acceptor_addrtype,
      acceptor_address: gss_buffer_desc_struct::from_sys(chan.acceptor_address),
      application_data: gss_buffer_desc_struct::from_sys(chan.application_data),
    }
  }
  pub fn from_sys_ptr(chan: gssapi_sys::gss_channel_bindings_t) -> gss_channel_bindings_struct {
    let chan: gssapi_sys::gss_channel_bindings_struct = unsafe{ *chan };
    gss_channel_bindings_struct::from_sys(chan)
  }
}
