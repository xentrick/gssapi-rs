use ::types::*;

pub const GSS_C_NO_NAME:             gss_name_t             = 0 as _;
pub const GSS_C_NO_BUFFER:           gss_buffer_t           = 0 as _;
pub const GSS_C_NO_OID:              gss_oid                = 0 as _;
pub const GSS_C_NO_OID_SET:          gss_oid_set            = 0 as _;
//pub const GSS_C_NO_CONTEXT:          gss_ctx_id_t           = 0 as _;
//pub const GSS_C_NO_CREDENTIAL:       gss_cred_id_t          = 0 as _;
//pub const GSS_C_NO_CHANNEL_BINDINGS: gss_channel_bindings_t = 0 as _;
pub const GSS_C_NULL_BUFFER:         gss_buffer_t           = 0 as _;
pub const GSS_C_NULL_OID:            gss_oid                = 0 as _;

pub const GSS_C_INDEFINITE: u32 = 0xffffffff;
pub const GSS_C_QOP_DEFAULT: gss_qop_t = 0;

//TODO is this supposed to be multiple setable bitflags or will we only see one at a time?
pub enum GSS_Context_Flag { // gss/api.h
  Delegate  = 1,
  Mutual    = 2,
  Replay    = 4,
  Sequence  = 8,
  Conf      = 16,  //configure?
  Integ     = 32,  // integrity?
  Anonymous = 64,
  ProtReady = 128, // protection?
  Trans     = 256, // trans...?
  // not gssapi compliant, for From conversions
  Invalid   = 512,
}
impl From<OM_uint32> for GSS_Context_Flag {
  fn from(val: OM_uint32) -> GSS_Context_Flag {
    match val {
      1   => GSS_Context_Flag::Delegate,
      2   => GSS_Context_Flag::Mutual,
      4   => GSS_Context_Flag::Replay,
      8   => GSS_Context_Flag::Sequence,
      16  => GSS_Context_Flag::Conf,
      32  => GSS_Context_Flag::Integ,
      64  => GSS_Context_Flag::Anonymous,
      128 => GSS_Context_Flag::ProtReady,
      256 => GSS_Context_Flag::Trans,
      _   => GSS_Context_Flag::Invalid,
    }
  }
}

pub enum GSS_Credential_Flag { // gss/api.h
  Both     = 0,
  Initiate = 1,
  Accept   = 2,
  // not gssapi compliant, for From conversions
  Invalid  = 0xff,
}
impl From<OM_uint32> for GSS_Credential_Flag {
  fn from(val: OM_uint32) -> GSS_Credential_Flag {
    match val {
      0 => GSS_Credential_Flag::Both,
      1 => GSS_Credential_Flag::Initiate,
      2 => GSS_Credential_Flag::Accept,
      _ => GSS_Credential_Flag::Invalid,
    }
  }
}

pub enum GSS_Display_Flag {
  GssCode  = 1,
  MechCode = 2,
  // not gssapi compliant, for From conversions
  Invalid  = 0xff,
}
impl From<OM_uint32> for GSS_Display_Flag {
  fn from(val: OM_uint32) -> GSS_Display_Flag {
    match val {
      1 => GSS_Display_Flag::GssCode,
      2 => GSS_Display_Flag::MechCode,
      _ => GSS_Display_Flag::Invalid,
    }
  }
}

pub enum GSS_Channel_Flag {
  Unspec    = 0,
  Local     = 1,
  Inet      = 2,
  Implink   = 3,
  Pup       = 4,
  Chaos     = 5,
  NS        = 6,
  NBS       = 7,
  ECMA      = 8,
  Datakit   = 9,
  CCITT     = 10,
  SNA       = 11,
  DECnet    = 12,
  DLI       = 13,
  LAT       = 14,
  Hylink    = 15,
  Appletalk = 16,
  BSC       = 17,
  DSS       = 18,
  OSI       = 19,
  X25       = 21,
  Null      = 255,
  // not gssapi compliant, for From conversions
  Invalid   = 256,
}
impl From<OM_uint32> for GSS_Channel_Flag {
  fn from(val: OM_uint32) -> GSS_Channel_Flag {
    match val {
      0 => GSS_Channel_Flag::Unspec,
      1 => GSS_Channel_Flag::Local,
      2 => GSS_Channel_Flag::Inet,
      3 => GSS_Channel_Flag::Implink,
      4 => GSS_Channel_Flag::Pup,
      5 => GSS_Channel_Flag::Chaos,
      6 => GSS_Channel_Flag::NS,
      7 => GSS_Channel_Flag::NBS,
      8 => GSS_Channel_Flag::ECMA,
      9 => GSS_Channel_Flag::Datakit,
      10 => GSS_Channel_Flag::CCITT,
      11 => GSS_Channel_Flag::SNA,
      12 => GSS_Channel_Flag::DECnet,
      13 => GSS_Channel_Flag::DLI,
      14 => GSS_Channel_Flag::LAT,
      15 => GSS_Channel_Flag::Hylink,
      16 => GSS_Channel_Flag::Appletalk,
      17 => GSS_Channel_Flag::BSC,
      18 => GSS_Channel_Flag::DSS,
      19 => GSS_Channel_Flag::OSI,
      21 => GSS_Channel_Flag::X25,
      255 => GSS_Channel_Flag::Null,
      _ => GSS_Channel_Flag::Invalid,
    }
  }
}

lazy_static! {
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_USER_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x01)
  );
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_MACHINE_UID_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x02)
  );
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_STRING_UID_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x03)
  );
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_HOSTBASED_SERVICE: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x04)
  );
  // deprecated
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_HOSTBASED_SERVICE_X: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2b, 0x06, 0x01, 0x05, 0x06, 0x02)
  );
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_ANONYMOUS: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2b, 0x06, 0x01, 0x05, 0x06, 0x03)
  );
  #[derive(Clone, Copy)]
  pub static ref GSS_C_NT_EXPORT_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2b, 0x06, 0x01, 0x05, 0x06, 0x04)
  );
}

// lib/headers/krb5.h
pub enum GSS_KRB5_Flag {
  BadServiceName = 1,
  BadStringUID = 2,
  NoUser = 3,
  ValidateFailed = 4,
  BufferAlloc = 5,
  BadMsgCTX = 6,
  WrongSize = 7,
  BadUsage = 8,
  UnknownQOP = 9,
  CCacheNoMatch = 10,
  KeyTabNoMatch = 11,
  TGTMissing = 12,
  NoSubKey = 13,
  ContextEstablished = 14,
  BadSignType = 15,
  BadLength = 16,
  CTXIncomplete = 17,
  // not gssapi compliant, for From conversions
  Invalid = 255
}

lazy_static!{
  // GSS_C_NT_USER_NAME
  pub static ref GSS_KRB5_NT_USER_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x01)
  );
  // GSS_C_NT_MACHINE_UID_NAME
  pub static ref GSS_KRB5_NT_MACHINE_UID_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x02)
  );
  // GSS_C_NT_STRING_UID_NAME
  pub static ref GSS_KRB5_NT_STRING_UID_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x03)
  );
  // use GSS_C_NT_HOSTBASED_SERVICE
  pub static ref GSS_KRB5_NT_HOSTBASED_SERVICE_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x04)
  );
  pub static ref GSS_KRB5_NT_PRINCIPAL_NAME: gss_oid_desc = gss_oid_desc::new(
    vec!(0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x02, 0x01)
  );

}
